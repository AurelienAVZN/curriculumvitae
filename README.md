# Curriculum Vitae
[![pipeline status](https://gitlab.com/AurelienAVZN/curriculumvitae/badges/master/pipeline.svg)](https://gitlab.com/AurelienAVZN/curriculumvitae/-/commits/master)

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.1.1.

## Configuration
- <img alt="NodeJS" src="https://img.shields.io/badge/node.js-%2343853D.svg?style=for-the-badge&logo=node-dot-js&logoColor=white"/> : Version 15 minimum
- <img alt="Angular" src="https://img.shields.io/badge/angular-%23DD0031.svg?style=for-the-badge&logo=angular&logoColor=white"/> : Version 12
- <img alt="Bootstrap" src="https://img.shields.io/badge/bootstrap-%23563D7C.svg?style=for-the-badge&logo=bootstrap&logoColor=white"/> : Version 4

## Development environment 
Using of visual studio code as IDE and the command npm for the management of node modules

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
Run `ng serve` for a dev server or `npm start` if you have the command installed.

## How works the repository

This repository have different types of branches, each with its own characteristics and functioning. Please take a look to the description below or on the dedicated wiki pages "Fonctionnement du git" (FR)

---

### Release Branches

The release branches represent a stable version of the project for the website part and the Anonymiseur part.\
In more of the branch, a real release of each will be present in the Seployments/Releases section.

**N.B : For now only one release branch is created when a new stable version for each part is available but in the future there will be a release branch separated for each part.**

---

### Dev Branches

The dev branches are separated by the part that we work on (Website or Anonymiseur). They represent a unstable version. It's not recommanded to take the code source from these branches but you can if you want.

---

### Master Branch

The master branch is only used to deploy my version on my curriculum vita on firebase and to recreate new dev branches is it's neccessary. 

## Notes about the Anonymiseur

For now the Anonymiseur is in the same repository  than the curriculvitae website but if this part take a important place with it code and it features, it will be separated on another specific repository
