package com.clap73.helper;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EmptySource;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;

import com.aav.helper.StringHelper;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Nested;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@DisplayName("Tests Unitaires - StringHelper")
public class StringHelperTests {
	
	@BeforeAll
	static void setEnvrionnement() {
		//TODO IF NECESSARY
	}
	
	@Nested
	@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
	@DisplayName("Test des proprietes de la classe")
	class UnitTestStaticProperty{
		@Order(1)
		@ParameterizedTest @EmptySource
		@DisplayName("StringHelper.Empty - verification")
		void testEmptyValue(String source) {
			assertEquals(source, StringHelper.Empty);
		}
	}
	
	@Nested
	@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
	@DisplayName("Tests des methodes utilisant des Strings")
	class UnitTestMethodTakingString{
		@ParameterizedTest
		@Order(1) @NullSource @EmptySource
		@DisplayName("StringHelper.IsNullOrWhiteSpace - Case True")
		@ValueSource( strings= {"       ", " "})
		void testIsNullOrWhiteSpace_True(String source) {
			assertTrue(StringHelper.IsNullOrWhiteSpace(source));
		}
		
		@Order(2)
		@ParameterizedTest
		@DisplayName("StringHelper.IsNullOrWhiteSpace - Case False")
		@ValueSource(strings= {"  t  ", "geropge", "n g  g"})
		void testIsNullOrWhiteSpace_False(String source) {
			assertFalse(StringHelper.IsNullOrWhiteSpace(source));
		}
		
		@Test @Order(3)
		@DisplayName("StringHelper.Concat - Case Working")
		void testConcat() {
			String[] values = new String[] {"Bonjour ", null, " ", "", "comment ça va ?"};
			String result = "Bonjour null comment ça va ?";
			assertEquals(result, StringHelper.Concat(values[0],values[1], values[2], values[3], values[4]));
		}
		
		@Order(4) @Test
		@DisplayName("StringHelper.IsMatchMail")
		void testRegex_With_SepRegex() {
			String[] emails = { "\"Fred Bloggs\"@example.com", "user@.invalid.com", "Chuck Norris <gmail@chucknorris.com>", "webmaster@müller.de", "matteo@78.47.122.114", "ge^rkgporkg" };
			boolean[] expected = { true, false, true, true, true, false };
			for(int i = 0; i < emails.length; i++) {
				assertEquals(StringHelper.IsMatchMail(emails[i]), expected[i]);
			}
		}
	}
	
	@Nested
	@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
	@DisplayName("Tests des methodes utilisant des charSequence")
	class UnitTestMethodTakingCharSequence{
		@ParameterizedTest
		@Order(1) @NullSource
		@DisplayName("StringHelper.IsNull - Case True")
		void test_IsNull_True(CharSequence source) {
			assertTrue(StringHelper.IsNull(source));
		}
		
		@Order(2)
		@ParameterizedTest
		@DisplayName("StringHelper.IsNull - Case False")
		@ValueSource(strings = {" ", "ferignir"})
		void test_IsNull_False(CharSequence source) {
			assertFalse(StringHelper.IsNull(source));
		}
		
		@ParameterizedTest
		@Order(3) @EmptySource @NullSource
		@DisplayName("StringHelper.IsNullOrEmpty - Case True")
		void test_IsNullOrEmpty_True(String source) {
			assertTrue(StringHelper.IsNullOrEmpty(source));
		}
		
		@Order(4)
		@ParameterizedTest
		@DisplayName("StringHelper.IsNullOrEmpty - Case False")
		@ValueSource( strings = {"  ", "egege", " \t  ", "/n"})
		void testIsNullOrEmpty_False(String source) {
			assertFalse(StringHelper.IsNullOrEmpty(source));
		}
	}
	
	@Nested
	@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
	@DisplayName("Tests autres methodes du StringHelper")
	class UnitTestOtherStringHelperMethod{
		@Order(1) @Test
		@DisplayName("StringHelper.getErrorMessage - Case Null Value")
		void testGetErrorMessage_NullValue() {
			assertThrows(NullPointerException.class, () -> {
				StringHelper.getErrorMessage(null);
			});
		}
		
		@Order(2) @Test
		@SuppressWarnings("null")
		@DisplayName("StringHelper.getErrorMessage - Case Worked")
		void testGetErrorMessage_CaseWorks() {
			try { String test = null; test.length(); }
			catch(NullPointerException npe) {
				String[] results = (StringHelper.getErrorMessage(npe)).split("\n");
				assertEquals(npe.getStackTrace().length, results.length);
				int i = 0;
				for(String result: results) {
					assertTrue(result.contains("at"));
					assertTrue(result.contains(npe.getStackTrace()[i].toString()));
					i++;
				}
			}
		}
	}
}
