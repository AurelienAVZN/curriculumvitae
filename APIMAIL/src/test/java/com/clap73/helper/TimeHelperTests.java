package com.clap73.helper;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import com.aav.helper.TimeHelper;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@DisplayName("Tests Unitaires - TimeHelper")
public class TimeHelperTests {
	@Test 
	@Order(1) @Tag("Method")
	@DisplayName("TimeHelper.Now - with format already")
	public void test_Now_With_Already_Format() {
		String[] values = TimeHelper.Now().split(" ");
		assertTrue(values.length == 2);
		assertTrue((values[0].split("/")).length == 3);
		assertTrue((values[1].split(":")).length == 3);
	}
}
