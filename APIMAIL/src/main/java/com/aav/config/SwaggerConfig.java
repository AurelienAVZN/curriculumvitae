package com.aav.config;

import java.util.Collections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import com.aav.config.properties.MailProperties;
import com.aav.helper.logs.LogsHelper;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@SuppressWarnings("deprecation")
@Configuration
@EnableSwagger2
public class SwaggerConfig {
	@Autowired
    private Environment env;
	
	@Bean
	public Docket swaggerConfiguration() {
		return new Docket(DocumentationType.SWAGGER_2).select().paths(PathSelectors.ant("/**"))
				.apis(RequestHandlerSelectors.basePackage("com.aav")).build().apiInfo(getApiInfo())
				.useDefaultResponseMessages(false);
	}
	
	@Bean
	public void mailProperties() {
		LogsHelper.info("Initialisation des configs mail...");
	    MailProperties.init(env.getProperty("spring.mail.host"), env.getProperty("spring.mail.port"), 
	    		env.getProperty("spring.mail.properties.mail.smtp.auth"), env.getProperty("spring.mail.properties.mail.smtp.starttls.enable"),
	    		env.getProperty("spring.mail.protocol"), env.getProperty("spring.mail.username"), env.getProperty("spring.mail.password"), 
	    		env.getProperty("spring.mail.properties.mail.smtp.ssl.trust"), env.getProperty("spring.mail.name"));
	}
	
	private ApiInfo getApiInfo() {
		return new ApiInfo(env.getProperty("swagger.name"), env.getProperty("swagger.label"), env.getProperty("swagger.version"),
				env.getProperty("swagger.termeofserviceurl"), new Contact(env.getProperty("swagger.contact.name"), env.getProperty("swagger.contact.url"), 
				env.getProperty("swagger.contact.mail")), env.getProperty("swagger.license.name"), env.getProperty("swagger.license.url"), Collections.emptyList());
	}

	@SuppressWarnings("deprecation")
	@Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**");
            }
        };
    }
}
