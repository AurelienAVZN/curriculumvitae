package com.aav.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
@EnableAutoConfiguration(exclude = { SecurityAutoConfiguration.class })
public class SecurityConfig extends WebSecurityConfigurerAdapter{
	@Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/v2/api-docs","/configuration/ui","/swagger-resources/**",
                                   "/configuration/security","/swagger-ui.html","/webjars/**", "/**");
    }
	
	@Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
		//Les requetes sont accessibles par n'importe qui
        httpSecurity.authorizeRequests().anyRequest().anonymous();
        httpSecurity.headers().cacheControl();
    }
}
