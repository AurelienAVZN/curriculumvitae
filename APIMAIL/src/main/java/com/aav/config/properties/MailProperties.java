package com.aav.config.properties;

import java.util.Properties;

import org.springframework.stereotype.Component;

@Component("MailConfig")
public class MailProperties {
	public static Properties properties;
	public static String host;
	public static String port;
	public static boolean isAuth;
	public static String startTls;
	public static String protocol;
	public static String username;
	public static String password;
	public static String ssl_trust;
	public static String name;
	
	public static void init(String _host, String _port, String _isAuth, String _startTls, 
			String _protocol, String _username, String _password, String _ssl_trust, String _name) {
		host = _host; port = _port;
		isAuth = _isAuth.equals("true") ? true : false; 
		startTls = _startTls;
		password = _password; username = _username;
		protocol = _protocol; ssl_trust = _ssl_trust;
		name = _name;
		setProperties();
		
	}
	
	private static void setProperties() {
		properties = new Properties();
		properties.put("mail.smtp.auth", isAuth);
		properties.put("mail.smtp.starttls.enable", startTls);
		properties.put("mail.smtp.host", host);
		properties.put("mail.smtp.port", port);
		//System.out.println("mail.smtp.ssl.trust :" + ssl_trust);
		//properties.put("mail.smtp.ssl.trust", ssl_trust);
	}
}
