package com.aav.api;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.aav.api.mail.MailRequest;
import com.aav.api.mail.MailResponse;
import com.aav.api.mail.MailSender;
import com.aav.helper.StringHelper;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.*;

@RestController
@Api(value="Controller which allows to send mail through SMTP")
public class MailController {
	
	@ApiOperation(value="Envoi un mail avec le contenue et le destinataire envoyé par le client")
	@ApiResponses(value = { 
		@ApiResponse(code = 200, message = "Mail sent successfully to SMTP server"),  
		@ApiResponse(code = 400, message = "The receiver mail address isn't valid"),
		@ApiResponse(code = 417, message = "An error occured during the mail sending process !"), 
		@ApiResponse(code = 500, message = "An unexpected error has occurred. The error has been logged and is being investigated.") 
	})
	@RequestMapping(method = RequestMethod.POST, value = "/mail/simplemail")
	public ResponseEntity<MailResponse> sendSimpleMail(@RequestBody MailRequest request) {
		if(!StringHelper.IsNullOrWhiteSpace(request.to) && StringHelper.IsMatchMail(request.to)) {
			MailSender sender = new MailSender();
			MailResponse response = sender.sendMail(request.to, request.subject, request.message, null, null);
			response.setMailData(request.subject, request.to, request.message);
			return ResponseEntity.status(response.getStatusCode()).body(response);
		}
		return ResponseEntity.badRequest().body(new MailResponse(HttpStatus.BAD_REQUEST, "The receiver mail address isn't valid"));		
	}
	
	@ApiOperation(value="Envoi un mail avec le contenue et le destinataire envoyé par le client")
	@ApiResponses(value = { 
		@ApiResponse(code = 200, message = "Mail sent successfully to SMTP server"),  
		@ApiResponse(code = 400, message = "The receiver mail address isn't valid"),
		@ApiResponse(code = 417, message = "An error occured during the mail sending process !"), 
		@ApiResponse(code = 500, message = "An unexpected error has occurred. The error has been logged and is being investigated.") 
	})
	@RequestMapping(method = RequestMethod.POST, value = "/mail/simplemail/accussereception")
	public ResponseEntity<MailResponse> sendMailWithAccuseReception(@RequestBody MailRequest request) {
		if(!StringHelper.IsNullOrWhiteSpace(request.toReception) && StringHelper.IsMatchMail(request.toReception)) {
			if(!StringHelper.IsNullOrWhiteSpace(request.to) && StringHelper.IsMatchMail(request.to)) {
				MailSender sender = new MailSender();
				MailResponse response = sender.sendMail(request.to, request.subject, request.message, null, null);
				response.setMailData(request.subject, request.to, request.message);
				sender.sendMail(request.toReception, request.subjectReception, request.messageReception, null, null);
				return ResponseEntity.status(response.getStatusCode()).body(response);
			}
		}
		return ResponseEntity.badRequest().body(new MailResponse(HttpStatus.BAD_REQUEST, "Your mail isn't valid !"));		
	}
}
