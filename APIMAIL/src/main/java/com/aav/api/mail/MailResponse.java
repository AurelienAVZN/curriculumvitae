package com.aav.api.mail;

import org.springframework.http.HttpStatus;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Response sent by the Mail controller")
public class MailResponse {
	@ApiModelProperty(notes = "The Http code to send")
	public HttpStatus code;
	@ApiModelProperty(notes = "Summary of the process executed by the controller")
	public String label;
	@ApiModelProperty(notes = "Exception if one of them occured during the process")
	public Throwable exception;
	@ApiModelProperty(notes = "The receiver mail address")
	public String to;
	@ApiModelProperty(notes = "Message sent by mail")
	public String message;
	@ApiModelProperty(notes = "Subject of the message sent !")
	public String subject;
	
	public MailResponse() { this(null, null, null); }
	
	public MailResponse(String label) { this(label, null, null); }
	
	public MailResponse(HttpStatus code, String label) { this(label, null, code); }
	
	public MailResponse(String label, Throwable exception, HttpStatus code) {
		this.label = label;
		this.exception = exception;
		this.code = code;
	}
	
	public HttpStatus getStatusCode() {
		if(code != null) return code;
		return exception == null ? HttpStatus.OK : HttpStatus.EXPECTATION_FAILED;
	}
	
	public void setReponse(String label, Throwable exception) {
		this.label = label;
		this.exception = exception;
	}
	
	public void setMailData(String subject, String to, String message) {
		this.message = message;
		this.subject = subject;
		this.to = to;
	}
}
