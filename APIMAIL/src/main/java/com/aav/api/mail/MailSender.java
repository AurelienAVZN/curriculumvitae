package com.aav.api.mail;

import static com.aav.helper.StringHelper.Concat;

import java.io.UnsupportedEncodingException;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.springframework.stereotype.Component;

import com.aav.config.properties.MailProperties;
import com.aav.helper.logs.LogsHelper;

/**
 * Class with all the methods needed to send a mail
 * to the SMTP server of the aeroclub
 * @version 1.0
 * @since
 */
@Component
public class MailSender {
	
	private Session session = null;
	
	public MailSender(){
		if(this.session == null) {
			this.session = Session.getInstance(MailProperties.properties, new Authenticator() {
			    @Override
			    protected PasswordAuthentication getPasswordAuthentication() {
			        return new PasswordAuthentication(MailProperties.username, MailProperties.password);
			    }
			});
		}
	}
	
	public MailResponse sendMail(String to, String subject, String message, String type, String charset) {
		MailResponse response = new MailResponse();
		LogsHelper.info(Concat("Sending mail : ", subject, " to ", to));
		try {
			Message mimemessage = this.setMimeMessage(to, subject, message, charset, type);
			LogsHelper.info("Trying connection to SMTP server...");
            Transport transport = this.session.getTransport(MailProperties.protocol);
            transport.connect(MailProperties.username, MailProperties.password);
				LogsHelper.info("Sending mail in progress...");
            Transport.send(mimemessage);
            LogsHelper.info("Mail sent successfully !");
            response.label = "Mail sent successfully !";
		}
		catch(Exception e) {
			LogsHelper.error("An error occured during the sending mail process !", e);
			response.setReponse("An error occured during the sending mail process !", e);
		}
		return response;
	}
	
	/**
	 * 
	 * @param to The receiver of the mail
	 * @param subject The subject of the mail
	 * @param message The message of the mail
	 * @param charset
	 * @param type
	 * @return The message to send through the SMTP server
	 * @throws UnsupportedEncodingException
	 * @throws MessagingException
	 */
	private Message setMimeMessage(String to, String subject, String message, String charset, String type) throws UnsupportedEncodingException, MessagingException {
		Message mimemessage = new MimeMessage(this.session);
		mimemessage.setFrom(new InternetAddress(MailProperties.username, MailProperties.name));
		mimemessage.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
		mimemessage.setSubject(subject);
		String type2 = type != null ? type  : "text/html";
		String charset2 = charset != null ? charset : "utf-8";
		MimeBodyPart mimeBodyPart = new MimeBodyPart();
		mimeBodyPart.setContent(message, type2 + "; charset=" + charset2);
		Multipart multipart = new MimeMultipart();
		multipart.addBodyPart(mimeBodyPart);
		mimemessage.setContent(multipart);
		return mimemessage;
	}
}