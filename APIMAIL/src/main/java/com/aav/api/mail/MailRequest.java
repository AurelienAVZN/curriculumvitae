package com.aav.api.mail;

import io.swagger.annotations.ApiModelProperty;

public class MailRequest {
	@ApiModelProperty(notes = "Subject of the message sent !")
	public String subject;
	@ApiModelProperty(notes = "Message sent by mail")
	public String message;
	@ApiModelProperty(notes = "The receiver mail address")
	public String to;
	@ApiModelProperty(notes = "Mail to send confirmation")
	public String toReception;
	@ApiModelProperty(notes = "")
	public String messageReception;
	@ApiModelProperty(notes = "")
	public String subjectReception;
	
	public MailRequest() {this(null, null, null, null, null, null); }
	
	public MailRequest(String to, String subject, String message) {this(to, null, subject, null, message, null); }
	
	public MailRequest(String to, String toReception, String subject, String subjectReception, String message, String messageReception) {
		this.to = to;
		this.toReception = toReception;
		this.subject = subject;
		this.subjectReception = subjectReception;
		this.message = message;
		this.messageReception = messageReception;
	}
}
