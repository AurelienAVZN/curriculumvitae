package com.aav.api;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Controller permettant d'avoir l'ensemble des informations
 * sur l'api en cours d'utilisation
 * @author Avanzino.A
 * @since 18/10/2021
 * @version 1.0
 */
@RestController
@Api(value="Controller permettant d'avoir l'ensembe des informations sur l'api en cours d'utilisation")
public class ApiController {
	@ApiOperation(value="Retourne la version actuelle de l'api")
	@RequestMapping(method = RequestMethod.GET, value = "/api/infos/version")
	public String getVersion() { return "1.0"; }
	
	@ApiOperation(value="Retourne la version actuelle de l'api")
	@RequestMapping(method = RequestMethod.GET, value = "/")
	public String serverRun() { return "Clap73 App Server is running !"; }
}
