package com.aav.helper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;

public class TimeHelper {
	/**
	 * Get the current date and time from the devices used
	 * @return the current date and time into the format YYYY/MM/DD HH:MM:SS
	 * @since 1.0
	 */
	public static String Now() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		return dateFormat.format(Date.from(Instant.now()));
	}
	
	public static String Now(String format) {
		DateFormat dateFormat = new SimpleDateFormat(format);
		return dateFormat.format(Date.from(Instant.now()));
	}
	
	public static String Now(DateFormat dateFormat) {
		return dateFormat.format(Date.from(Instant.now()));
	}
	
	public static String SqlDateToString(java.sql.Date date) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return dateFormat.format(date);
	}
}
