package com.aav.helper.logs;

public enum LogLevel implements ILogLevel{
	INFO("INFO"), ERROR("ERROR"), WARNING("WARNING"), DEBUG("DEBUG");

	private String code;

	LogLevel(String code) { this.code = code; }

	public String getCode() { return code; }
	
	public static LogLevel fromValue(String code) {
		for(LogLevel lvl: LogLevel.values()) {
			if(lvl.code == code) { return lvl; }
		}
		return null;
	}
}
