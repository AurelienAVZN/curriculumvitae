package com.aav.helper.logs;

import static com.aav.helper.StringHelper.getErrorMessage;

import com.aav.helper.TimeHelper;

public class LogsHelper {
	public static void info(CharSequence seq) { log(LogLevel.INFO, seq); }
	
	public static void info(Throwable e) { log(LogLevel.INFO, e); }
	
	public static void info(CharSequence seq, Throwable e) { log(LogLevel.INFO, seq, e); }
	
	public static void warning(CharSequence seq) { log(LogLevel.WARNING, seq); }
	
	public static void warning(Throwable e) { log(LogLevel.WARNING, e); }
	
	public static void warning(CharSequence seq, Throwable e) { log(LogLevel.WARNING, seq, e); }

	public static void error(CharSequence seq) { log(LogLevel.ERROR, seq); }

	public static void error(Throwable e) { log(LogLevel.ERROR, e); }

	public static void error(CharSequence seq, Throwable e) { log(LogLevel.ERROR, seq, e); }
	
	public static void log(ILogLevel level, Throwable e) {
		log(level, "", e);
	}

	public static void log(ILogLevel level, CharSequence seq) {
		CharSequence message = TimeHelper.Now() + " - " + level.getCode() + " : " + seq;
		System.out.println(message);
		//if (logsConfig.ActivateElement) write(message + "\n");
	}

	public static void log(ILogLevel level, CharSequence seq, Throwable e) {
		log(level, seq + "\n" + getErrorMessage(e));
	}
}
