package com.aav;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication(exclude = {SecurityAutoConfiguration.class })
@PropertySource({"classpath:mail.properties", "classpath:swagger.properties"})
public class Clap73ServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(Clap73ServerApplication.class, args);
	}
}
