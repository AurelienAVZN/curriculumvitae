/**
 * @description
 * @author Aurélien Avanzino
 * @version 1.3
 * @since
 */
export enum LangPathEnum {
    MENU = "page/menu.json",
    HOME = "page/home.json",
    CONNECT = "page/connect.json",
    ABOUT = "page/propos.json",
    CONTACT = "page/contact.json",
    PROFIL = "page/profil.json",
    ERROR = "page/error.json",
    SOCIAL = "socials.json",
    FLAG = "lang.json",
    //DONNEES DU CV
    DIPLOMES = "CV/Diplomes.json",
    CENTRES = "CV/Centres.json",
    EXPERIENCES = "CV/Experiences.json",
    CATEGORY = "CV/Categories.json",
    ENTETE = "page/infos.json"
    //
}
