export enum TypeCompetences {
    INFORMATIQUE = "Informatique",
    FRAMEWORK = "Framework",
    LOGICIELS = "Logiciels",
    LANGUES = "Langues"
}
