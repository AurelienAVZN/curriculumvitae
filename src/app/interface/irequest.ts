export interface IMailRequest {
   to: string,
   subject: string,
   message: string,
   messageReception: string,
   toReception: string,
   subjectReception: string
}