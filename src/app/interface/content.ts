/**
 * @description Permet de generer le menu du site
 * @author Aurélien Avanzino
 * @version 1.1
 * @since 30/01/2022
 */
export interface IMenu {
   id: number,
   name: string,
   route: string | undefined,
   subroute: IMenu[] | undefined,
   idclass: string | undefined
}

/**
 * @description Permet de générer les liens vers les
 * différents réseaux sociaux
 * @author Aurélien Avanzino
 * @version 1.0
 * @since 30/01/2022
 */
export interface ISocial {
   id: number,
   name: string,
   route: string | undefined,
   image : string
}

/**
 * @description Permet de gérer le texte de la page
 * contact
 * @author Aurélien Avanzino
 * @version 1.0
 * @since 30/01/2022
 */
export interface IContact{
   title: string,
   infoapi: string,
   warning: string,
   nom: string,
   prenom: string,
   mail: string,
   sujet: string,
   message: string,
   obligatoire: string,
   send: string,
   texte: string
}

/**
 * @description Permet de gérer l'affichage des
 * modifications du site
 * @author Aurélien Avanzino
 * @version 1.0
 * @since 30/01/2022
 */
export interface IPropos{
   id: number,
   version: string,
   subversion: IPropos[]
}

/**
 * @description Permet de gérer le menu des langues
 * @author Aurélien Avanzino
 * @version 1.0
 * @since 31/01/2022
 */
export interface IFlag{
   id: number,
   picture: string,
   lang: string,
   title: string
}




 

 
 export interface IError{
    code: number,
    description: string
 }
 