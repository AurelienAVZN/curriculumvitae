export interface ICompetences {
    id: number,
    name: string,
    level: number,
    type: string
}

export interface IExperiences {
    id: number,
    title : string,
    entite: string,
    lieu: string,
    dateDebut: string,
    dateFin : string | undefined,
    description: string | undefined
}

export interface ICentres{
    id: number,
    title : string,
    description: string
}

export interface ICategory{
    title: string,
    values: ITuple[]
}

export interface ITuple{
    title: string,
    infos?: string
}