/**
 * @description Contient l'ensemble des interfaces utilisées pour 
 * manipuler les données présentes sur la pages accueil (aka CV)
 * Sauf les infos concernant les expériences/diplomes et centre interet
 * @author Avanzino Aurélien
 * @copyright Avanzino Aurélien
 * @version 1.0
 */

/**
 * @description Interface l'ensemble des infos de l'entetes
 * @author Avanzino Aurélien
 * @copyright Avanzino Aurélien
 */
export interface IEntete {
    entete: IPresentation,
    infos: ICoordonnees[]
}

/**
 * @description
 * @author Avanzino Aurélien
 * @copyright Avanzino Aurélien
 */
export interface ICoordonnees{
    id: string,
    value: string
}

/**
 * @description Interface pour affichage des titres du CV
 * @author Avanzino Aurélien
 * @copyright Avanzino Aurélien
 */
export interface IPresentation {
    nom: string,
    intitule: string[]
}