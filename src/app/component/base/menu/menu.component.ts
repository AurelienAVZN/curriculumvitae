import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { LangPathEnum } from 'src/app/enum/lang-path-enum';
import { IFlag, IMenu, ISocial } from 'src/app/interface/content';
import { LangageService } from 'src/app/service/langage.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent {
  menu$: Observable<IMenu[]>;
  social$: Observable<ISocial[]>;
  lang$: Observable<IFlag[]>;
  isMenuCollapsed = true;
  currentPicture: string | null;


  constructor(private lang: LangageService) { 
    this.menu$ = this.lang.get<IMenu[]>(LangPathEnum.MENU);
    this.social$ = this.lang.data<ISocial[]>(LangPathEnum.SOCIAL);
    this.lang$ = this.lang.data<IFlag[]>(LangPathEnum.FLAG);
    this.currentPicture = localStorage.getItem("langPic") !== null
    ? localStorage.getItem("langPic") 
    : this.lang.basePath + "/pictures/flags/" + this.lang.langage + "flag.jpg";
    //console.log(this.currentPicture);
  }

  public hasSubRoute(itemMenu: IMenu): boolean{
    return itemMenu?.subroute === undefined ? false : true;
  }

  public language(lang: IFlag){ 
    this.lang.langage = lang.lang;
    this.currentPicture = lang.picture;
    localStorage.setItem("langPic", lang.picture);
    this.menu$ = this.lang.get<IMenu[]>(LangPathEnum.MENU);
    window.location.reload();
  }

  public picture(): string | null { return this.currentPicture; }
}
