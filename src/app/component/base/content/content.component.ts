import { Component, Input } from '@angular/core';
import { IExperiences } from 'src/app/interface/cv';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent {

  @Input() liste: IExperiences | undefined;

  constructor() { }
}
