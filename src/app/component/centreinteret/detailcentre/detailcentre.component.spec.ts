import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailcentreComponent } from './detailcentre.component';

describe('DetailcentreComponent', () => {
  let component: DetailcentreComponent;
  let fixture: ComponentFixture<DetailcentreComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailcentreComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailcentreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
