import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { LangPathEnum } from 'src/app/enum/lang-path-enum';
import { IPropos } from 'src/app/interface/content';
import { LangageService } from 'src/app/service/langage.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent {
  propos$: Observable<IPropos[]>

  constructor(private lang: LangageService) { 
    this.propos$ = this.lang.get<IPropos[]>(LangPathEnum.ABOUT);
  }
}
