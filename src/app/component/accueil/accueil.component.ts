import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { LangPathEnum } from 'src/app/enum/lang-path-enum';
import { ICategory, IExperiences } from 'src/app/interface/cv';
import { IEntete } from 'src/app/interface/infos';
import { LangageService } from 'src/app/service/langage.service';

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.scss']
})
export class AccueilComponent{

  entete$: Observable<IEntete>
  diplomes$: Observable<IExperiences[]>;
  experiences$: Observable<IExperiences[]>;
  categorie$: Observable<ICategory[]>;

  constructor(private lang: LangageService) { 
    this.diplomes$ = this.lang.get(LangPathEnum.DIPLOMES);
    this.experiences$ = this.lang.get(LangPathEnum.EXPERIENCES);
    this.categorie$ = this.lang.get(LangPathEnum.CATEGORY);
    this.entete$ = this.lang.get(LangPathEnum.ENTETE);
  }
}
