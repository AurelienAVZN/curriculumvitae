import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { MailControllerService, MailRequest, MailResponse } from 'src/app/APIMAILCLIENT';
import { LangPathEnum } from 'src/app/enum/lang-path-enum';
import { IContact, ISocial } from 'src/app/interface/content';
import { LangageService } from 'src/app/service/langage.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent{
  //#region BINDING PROPERTY
  public email: string = "";
  public subject: string = "";
  public nom: string = "";
  public prenom: string = "";
  public message: string= "";
  //#endregion

  contact$: Observable<IContact>;
  social$: Observable<ISocial[]>;
  private mymail: string = "cita.avan@gmail.com";
  private subjectReception: string = "CV Aurélien - Accusé de reception";
  private isError: boolean = false;
  private isSent: boolean = false;

  constructor(private lang: LangageService, private mailController: MailControllerService) { 
    this.contact$ = this.lang.get<IContact>(LangPathEnum.CONTACT);
    this.social$ = this.lang.data<ISocial[]>(LangPathEnum.SOCIAL);
  }

  public async sendMail(): Promise<void>{
    console.log(this.message);
    let request: MailRequest = {
      to: this.mymail,
      subject: "APIMAIL : " + this.subject,
      message: this.createMessage(this.message, this.nom, this.prenom, this.email, this.subject),
      toReception: this.email,
      messageReception: this.createAccuseMessage(this.message, this.nom, this.prenom, this.email, this.subject),
      subjectReception: this.subject
    }
    console.log(request);
    this.mailController.sendSimpleMailUsingPOST(request).subscribe((response: MailResponse) => {
      console.log(response);
    });
  }

  private createMessage(message: string, nom: string, prenom: string, toReception: string, subject: string) : string {
    return `<h5>Mail CV en ligne !</h5>
    <p>Un mail a &eacute;t&eacute; envoy&eacute; depuis la version web du curriculum vitae !</p>
    <table style="border-collapse: collapse; width: 99.858%; height: 90px;" border="1">
    <tbody>
    <tr style="height: 18px;">
    <td style="width: 23.5795%; height: 18px;">Nom Prenom</td>
    <td style="width: 76.2785%; height: 18px;">${nom} ${prenom}</td>
    </tr>
    <tr style="height: 18px;">
    <td style="width: 23.5795%; height: 18px;">Adresse Mail</td>
    <td style="width: 76.2785%; height: 18px;">${toReception}</td>
    </tr>
    <tr style="height: 18px;">
    <td style="width: 23.5795%; height: 18px;">Date d'envoi</td>
    <td style="width: 76.2785%; height: 18px;">${(new Date()).toDateString()}</td>
    </tr>
    <tr style="height: 18px;">
    <td style="width: 23.5795%; height: 18px;">Sujet</td>
    <td style="width: 76.2785%; height: 18px;">${subject}</td>
    </tr>
    <tr style="height: 18px;">
    <td style="width: 23.5795%; height: 18px;">Contenu du message</td>
    <td style="width: 76.2785%; height: 18px;">${message}</td>
    </tr>
    </tbody>
    </table>
    <p><em><sub>Mail envoy&eacute; depuis l'api APIMAIL</sub></em></p>`;
  }

  private createAccuseMessage(message: string, nom: string, prenom: string, toReception: string, subject: string): string {
    return `<h4>Confirmation d'envoi de mail</h4>
    <p>Bonjour ${nom} ${prenom},</p>
    <p>Ce mail vous a &eacute;t&eacute; envoy&eacute; afin de confirmer l'envoi du formulaire effectu&eacute; depuis mon curriculum vitae en ligne.<br />Vous trouverez ci-dessous un r&eacute;capitulatif des informations que vous avez renseign&eacute;.&nbsp;</p>
    <table style="border-collapse: collapse; width: 99.858%; height: 90px;" border="1">
    <tbody>
    <tr style="height: 18px;">
    <td style="width: 23.5795%; height: 18px;">Nom Prenom</td>
    <td style="width: 76.2785%; height: 18px;">${nom} ${prenom}</td>
    </tr>
    <tr style="height: 18px;">
    <td style="width: 23.5795%; height: 18px;">Adresse Mail</td>
    <td style="width: 76.2785%; height: 18px;">${toReception}</td>
    </tr>
    <tr style="height: 18px;">
    <td style="width: 23.5795%; height: 18px;">Date d'envoi</td>
    <td style="width: 76.2785%; height: 18px;">${(new Date()).toDateString()}</td>
    </tr>
    <tr style="height: 18px;">
    <td style="width: 23.5795%; height: 18px;">Sujet</td>
    <td style="width: 76.2785%; height: 18px;">${subject}</td>
    </tr>
    <tr style="height: 18px;">
    <td style="width: 23.5795%; height: 18px;">Contenu du message</td>
    <td style="width: 76.2785%; height: 18px;">${message}</td>
    </tr>
    </tbody>
    </table>
    <p>Si jamais vous ne recevez pas de r&eacute;ponse de ma part au bout d'un certain temps n'h&eacute;sitez pas &agrave; me contacter via mon adresse mail ou via mes r&eacute;seaux sociaux</p>
    <p>Cordialement,<br />AVANZINO Aur&eacute;lien</p>
    <p><em><sub>Mail envoy&eacute; depuis l'api APIMAIL</sub></em></p>`;
  }
}
