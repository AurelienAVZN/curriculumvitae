export enum AppRoutingEnum {
    HOME = "",
    EXPERIENCES = "experiences",
    DIPLOME = "diplomes",
    PROJETS = "projets",
    PROJETSDETAILS = "projets/details",
    CENTRES = "centresInterets",
    CENTREDETAIL = "centresInterets/detail",
    ABOUT = "about",
    CONTACT = "contactme",
    NOTFOUND = "error",
    COMPETENCES = "competences"
}
