import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppRoutingEnum } from './app-routing-enum';
import { AboutComponent } from './component/about/about.component';
import { AccueilComponent } from './component/accueil/accueil.component';
import { PageErrorComponent } from './component/base/page-error/page-error.component';
import { DetailcentreComponent } from './component/centreinteret/detailcentre/detailcentre.component';
import { ListcentreComponent } from './component/centreinteret/listcentre/listcentre.component';
import { ListcompComponent } from './component/competences/listcomp/listcomp.component';
import { ContactComponent } from './component/contact/contact.component';
import { DiplomeComponent } from './component/experience/diplome/diplome.component';
import { ExpproComponent } from './component/experience/exppro/exppro.component';
import { DetailsComponent } from './component/projet/details/details.component';
import { ListprojetComponent } from './component/projet/listprojet/listprojet.component';

const routes: Routes = [
  {path: AppRoutingEnum.HOME, component: AccueilComponent},
  {path: AppRoutingEnum.CONTACT, component: ContactComponent},
  {path: AppRoutingEnum.EXPERIENCES, component: ExpproComponent},
  {path: AppRoutingEnum.DIPLOME, component: DiplomeComponent},
  {path: AppRoutingEnum.PROJETS, component: ListprojetComponent},
  {path: AppRoutingEnum.PROJETSDETAILS, component: DetailsComponent},
  {path: AppRoutingEnum.CENTRES, component: ListcentreComponent},
  {path: AppRoutingEnum.CENTREDETAIL, component: DetailcentreComponent},
  {path: AppRoutingEnum.ABOUT, component: AboutComponent},
  {path: AppRoutingEnum.NOTFOUND, component: PageErrorComponent},
  {path: AppRoutingEnum.COMPETENCES, component: ListcompComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
