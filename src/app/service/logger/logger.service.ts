import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { LogLevel } from './log-level';

@Injectable({
  providedIn: 'root'
})
export class LoggerService {

  constructor() { 

  }

  set debug(value: string){
    if(!environment.production){
      console.log(LogLevel.DEBUG + " : " + value);
    }
  }

  set info(value: string){ console.log(LogLevel.INFO + " : " + value); }

  set warning(value: string){ console.log(LogLevel.WARNING + " : " + value); }

  set error(value: string){ console.log(LogLevel.ERROR + " : " + value); }
}
