import { Injectable } from '@angular/core';
import { LangageService } from './langage.service';

@Injectable({
  providedIn: 'root'
})
export class ComponentManagerService {
  private seemenu: boolean;
  private _version: string;

  constructor(private lang: LangageService) { 
    this.seemenu = true;
    this._version = "";
  }

  get seeMenu(){ return this.seemenu;}
  set seeMenu(value: boolean){ this.seemenu = value;}

  get version(){ return this._version; }
  set version(value: string){ this._version = value; }
}
