import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { LangPathEnum } from '../enum/lang-path-enum';

@Injectable({
  providedIn: 'root'
})
export class LangageService {
  private _langage: string = "";
  private _basePath: string = "./assets/data/";
  private _path : string = "";

  constructor(private http: HttpClient) { 
    this.langage = localStorage.getItem("CVLangage");
    console.log(this._path);
  }

  get langage(): string { return this._langage; }

  get basePath(): string { return this._basePath; }

  set langage(value: string | null){ 
    this._langage = value !== null ? value : navigator.language; 
    localStorage.setItem("CVLangage", this._langage);
    this._path = this._basePath + this._langage + "/";
  }

  get<T>(filename: LangPathEnum): Observable<T>{
    return this.http.get<T>(this._path + filename);
  }

  data<T>(filename: LangPathEnum): Observable<T>{
    return this.http.get<T>(this._basePath + filename);
  }
}