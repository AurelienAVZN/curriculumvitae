export * from './apiController.service';
import { ApiControllerService } from './apiController.service';
export * from './mailController.service';
import { MailControllerService } from './mailController.service';
export const APIS = [ApiControllerService, MailControllerService];
