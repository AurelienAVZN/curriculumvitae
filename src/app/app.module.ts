import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AccueilComponent } from './component/accueil/accueil.component';
import { ContactComponent } from './component/contact/contact.component';
import { MenuComponent } from './component/base/menu/menu.component';
import { DetailsComponent } from './component/projet/details/details.component';
import { environment } from 'src/environments/environment.prod';
//import { AngularFireModule } from '@angular/fire';
//import { AngularFireAuthModule } from '@angular/fire/auth';
import { HttpClientModule} from '@angular/common/http';
import { AboutComponent } from './component/about/about.component';
import { FooterComponent } from './component/base/footer/footer.component';
import { PageErrorComponent } from './component/base/page-error/page-error.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MarkdownModule } from 'ngx-markdown';
import { ListprojetComponent } from './component/projet/listprojet/listprojet.component';
import { ListcentreComponent } from './component/centreinteret/listcentre/listcentre.component';
import { DetailcentreComponent } from './component/centreinteret/detailcentre/detailcentre.component';
import { DetailexpComponent } from './component/experience/detailexp/detailexp.component';
import { DiplomeComponent } from './component/experience/diplome/diplome.component';
import { ExpproComponent } from './component/experience/exppro/exppro.component';
import { ListcompComponent } from './component/competences/listcomp/listcomp.component';

import { ApiModule, BASE_PATH } from './APIMAILCLIENT';
import { ContentComponent } from './component/base/content/content.component';

@NgModule({
  declarations: [
    AppComponent,
    AccueilComponent,
    ContactComponent,
    MenuComponent,
    DetailsComponent,
    AboutComponent,
    FooterComponent,
    PageErrorComponent,
    ListprojetComponent,
    ListcentreComponent,
    DetailcentreComponent,
    DetailexpComponent,
    DiplomeComponent,
    ExpproComponent,
    ListcompComponent,
    ContentComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    //AngularFireModule.initializeApp(environment.firebase),
    //AngularFireAuthModule,
    ApiModule,
    AppRoutingModule,
    NgbModule,
    MarkdownModule.forRoot()
  ],
  providers: [{ provide: BASE_PATH, useValue: "http://localhost:8080" }],
  bootstrap: [AppComponent]
})
export class AppModule { }

//http://localhost:8080
//https://cv-api-aav.herokuapp.com